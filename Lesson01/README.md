HighlightLine - 高亮你的代码行
====================

## Highlight Line 插件

https://marketplace.visualstudio.com/vscode

## 安装

从VSCode环境中进行安装。

## 设置

* highlightLine.borderStyle
* highlightLine.borderWidth
* highlightLine.borderColor

## 课程文件

https://gitee.com/komavideo/LearnVSCode

## 小马视频频道

http://komavideo.com
